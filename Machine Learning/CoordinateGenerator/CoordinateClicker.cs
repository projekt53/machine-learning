﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoordinateGenerator
{
    public sealed partial class CoordinateClicker : Form
    {
        public List<Point> Points; 
        public CoordinateClicker(double minimumX, double maximumX, double minimumY, double maximumY)
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            labelRange.Text = $" X: {minimumX} - {maximumX}; Y: {minimumY} - {maximumY};";
            Points = new List<Point>();
        }
        private void labelRange_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            Invalidate();

        }

        private void CoordinateClicker_MouseClick(object sender, MouseEventArgs e)
        {
            var point = new Point(e.X-this.Width/2, e.Y-this.Height/2);
            Points.Add(point);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Points.Count > 0)
            {
                Points.Remove(Points.Last());
            }
        }

        private void CoordinateClicker_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Black, 0, this.Height / 2, this.Width, this.Height / 2);
            e.Graphics.DrawLine(Pens.Black, this.Width / 2, 0, this.Width / 2, this.Height);
            e.Graphics.DrawString("0,0", DefaultFont, Brushes.Black, (this.Width / 2) + 5, (this.Height / 2) - 15);
            foreach (var point in Points)
            {
                e.Graphics.FillEllipse(Brushes.Red, point.X+Width/2 - 2, point.Y+Height/2 - 2, 4, 4);
            }
        }
    }
}
