﻿using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Machine_Learning
{
    public class FeatureNormalization
    {
        public static Vector<double> NormalizeFeatures(Vector<double> features)
        {
            var maxValue = features.Max();
            var minValue = features.Min();
            var totalValue = features.Sum();
            var average = totalValue / features.Count;

            for (var i = 0; i < features.Count; i++)
                features[i] = (features[i] - average) / (maxValue - minValue);

            return features;
        }
        public static Matrix<double> NormalizeFeatures(Matrix<double> features)
        {
            var m = new DenseMatrix(features.RowCount, features.ColumnCount);
            for (var i = 0; i < features.ColumnCount; i++)
                m.SetColumn(i, NormalizeFeatures(features.Column(i)));

            return m;
        }
    }
}
