﻿namespace Machine_Learning
{
    partial class DataPlotter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.l_eq = new System.Windows.Forms.Label();
            this.plotDataControl1 = new Machine_Learning.PlotDataControl();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPrediction = new System.Windows.Forms.Label();
            this.textBoxPredict = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.ShowAlways = true;
            // 
            // l_eq
            // 
            this.l_eq.AutoSize = true;
            this.l_eq.Location = new System.Drawing.Point(4, 4);
            this.l_eq.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.l_eq.Name = "l_eq";
            this.l_eq.Size = new System.Drawing.Size(0, 13);
            this.l_eq.TabIndex = 0;
            // 
            // plotDataControl1
            // 
            this.plotDataControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plotDataControl1.LinesCount = 100;
            this.plotDataControl1.Location = new System.Drawing.Point(12, 12);
            this.plotDataControl1.Name = "plotDataControl1";
            this.plotDataControl1.R = 5F;
            this.plotDataControl1.Size = new System.Drawing.Size(773, 667);
            this.plotDataControl1.TabIndex = 1;
            this.plotDataControl1.Theta = null;
            this.plotDataControl1.X = null;
            this.plotDataControl1.Y = null;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Predict";
            // 
            // labelPrediction
            // 
            this.labelPrediction.AutoSize = true;
            this.labelPrediction.Location = new System.Drawing.Point(9, 36);
            this.labelPrediction.Name = "labelPrediction";
            this.labelPrediction.Size = new System.Drawing.Size(0, 13);
            this.labelPrediction.TabIndex = 3;
            // 
            // textBoxPredict
            // 
            this.textBoxPredict.Location = new System.Drawing.Point(54, 6);
            this.textBoxPredict.Name = "textBoxPredict";
            this.textBoxPredict.Size = new System.Drawing.Size(100, 20);
            this.textBoxPredict.TabIndex = 4;
            this.textBoxPredict.TextChanged += new System.EventHandler(this.textBoxPredict_TextChanged);
            // 
            // DataPlotter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(797, 691);
            this.Controls.Add(this.textBoxPredict);
            this.Controls.Add(this.labelPrediction);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.plotDataControl1);
            this.Controls.Add(this.l_eq);
            this.Name = "DataPlotter";
            this.Text = "DataPlotter";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DataPlotter_FormClosed);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ShowData_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label l_eq;
        private PlotDataControl plotDataControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelPrediction;
        private System.Windows.Forms.TextBox textBoxPredict;
    }
}