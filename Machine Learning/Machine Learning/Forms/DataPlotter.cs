﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    public partial class DataPlotter : Form
    {
        public DataPlotter(Vector<double> X, Vector<double> Y, Vector<double> Theta, Regresion reg)
        {
            InitializeComponent();
            plotDataControl1.SetX(X);
            plotDataControl1.SetY(Y);
            plotDataControl1.Theta = Theta;
            plotDataControl1.Regresion = reg;
        }

        public DataPlotter(Matrix<double> m, Regresion reg)
        {
            InitializeComponent();
            plotDataControl1.SetMatrix(m);
            plotDataControl1.Regresion = reg;
        }

        private void ShowData_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(this, plotDataControl1.GetRealPointF(e.Location.X, e.Location.Y).ToString());
        }

        public void UpdateTheta(Vector<double> theta)
        {
            if (InvokeRequired)
            {
                plotDataControl1.Invoke(new Action(() =>
                {
                    plotDataControl1.Theta = theta;
                    plotDataControl1.Refresh();
                }));
            }
            else
            {
                plotDataControl1.Theta = theta;
                plotDataControl1.Refresh();
            }
        }

        private void textBoxPredict_TextChanged(object sender, EventArgs e)
        {
            var textBox = (TextBox) sender;
            try
            {
                double value = Convert.ToDouble(textBox.Text);
                Vector<double> v = Vector<double>.Build.Dense(1, value);
                labelPrediction.Text = plotDataControl1.Regresion.Hypothesis(v, plotDataControl1.Theta).ToString();
            }
            catch (Exception)
            {
                labelPrediction.Text = "NaN";

            }
         
        }

        private void DataPlotter_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }

}
