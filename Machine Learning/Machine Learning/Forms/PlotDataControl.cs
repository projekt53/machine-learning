﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Machine_Learning
{
    public partial class PlotDataControl : UserControl
    {
        #region private vars
        private Matrix<double> _x;
        private Vector<double> _y;
        private float minX1;
        private float maxX1;
        private float minX2;
        private float maxX2;
        private float minY;
        private float maxY;
        private bool mode3D;
        #endregion

        public bool Mode3D
        {
            get
            {
                return mode3D;
            }

            private set
            {
                mode3D = value;
            }
        }
        public float R { get; set; } = 5f;
        public int LinesCount { get; set; } = 100;
        public Matrix<double> X
        {
            get { return _x; }
            set
            {
                _x = value;
                if (_x == null)
                {
                    minX1 = float.MinValue;
                    maxX1 = float.MaxValue;
                    minX2 = float.MinValue;
                    maxX2 = float.MaxValue;
                    return;
                }
                if(_x.ColumnCount > 2)
                    throw new Exception("x matrix is too big");
                if (_x.ColumnCount == 2 != mode3D)
                {
                    mode3D = !mode3D;
                    UpdateYsizes();
                }
                UpdateXsizes();



            }
        }
        public Vector<double> Y
        {
            get { return _y; }
            set
            {
                _y = value;
                if (_y == null)
                {
                    minX2 = float.MinValue;
                    maxX2 = float.MaxValue;
                    return;
                }
                if (_x.ColumnCount == 2 != mode3D)
                {
                    mode3D = !mode3D;
                    UpdateXsizes();
                }
                UpdateYsizes();
            }
        }
        public Vector<double> Theta { get; set; }
        public Regresion Regresion;

        public void SetX(Vector<double> x) => X = x.ToColumnMatrix();
        public void SetX(Matrix<double> x) => X = x;
        public void SetY(Vector<double> y) => Y = y;

        public void SetMatrix(Matrix<double> m)
        {
            if(m.ColumnCount < 2)
                throw new Exception("matrix is too small");
            X = m.RemoveColumn(m.ColumnCount - 1);
            Y = m.Column(m.ColumnCount - 1);
        }

        private void UpdateXsizes()
        {
            if (_x == null)
            {
                minX1 = float.MinValue;
                maxX1 = float.MaxValue;
                minX2 = float.MinValue;
                maxX2 = float.MaxValue;
                return;
            }

            if (_x.ColumnCount == 2) //3D mode
            {
                minX1 = (float)_x.Column(0).Min();
                maxX1 = (float)_x.Column(0).Max();
                minX2 = (float)_x.Column(1).Min();
                maxX2 = (float)_x.Column(1).Max();

                minX1 -= 0.1f * Math.Abs(minX1);
                maxX1 += 0.1f * Math.Abs(maxX1);
                maxX2 += 0.1f * Math.Abs(maxX2);
                minX2 -= 0.1f * Math.Abs(minX2);
            }
            else if (_x.ColumnCount == 1) //2D mode
            {
                minX1 = (float)_x.Column(0).Min();
                maxX1 = (float)_x.Column(0).Max();
                minX1 -= 0.1f * Math.Abs(minX1);
                maxX1 += 0.1f * Math.Abs(maxX1);
            }
            else //rip mode
                throw new Exception("x matrix is empty");
        }
        private void UpdateYsizes()
        {
            if (_y == null)
            {
                minX2 = float.MinValue;
                maxX2 = float.MaxValue;
                minY = float.MinValue;
                maxY = float.MaxValue;
                return;
            }
            if (mode3D) //3D
            {
                minY = (float)_y.Min();
                maxY = (float)_y.Max();
            }
            else //2D
            {
                minY = float.MinValue;
                maxY = float.MaxValue;
                minX2 = (float)_y.Min();
                maxX2 = (float)_y.Max();
                maxX2 += 0.1f * Math.Abs(maxX2);
                minX2 -= 0.1f * Math.Abs(minX2);
            }
        }

        public PlotDataControl()
        {
            InitializeComponent();
            this.ResizeRedraw = true;
            DoubleBuffered = true;
        }

        private void PlotData2D_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = SmoothingMode.HighQuality;

            g.Clear(Color.White);

            if (X == null || Y == null)
                return;

            //Draw all points
            if(mode3D)
            for (var i = 0; i < X.RowCount; i++)
                g.FillEllipse(new SolidBrush(Color.FromArgb((int)Y[i].Map(minY,maxY,0,255),0, (int)Y[i].Map(minY, maxY, 255, 0))), (float)X[i,0].Map(minX1, maxX1, 0, (float)ClientSize.Width) - R, (float)X[i,1].Map(minX2, maxX2, (float)ClientSize.Height, 0) - R, R, R);
            else
                for (var i = 0; i < X.RowCount; i++)
                    g.FillEllipse(Brushes.Red, (float)X[i,0].Map(minX1, maxX1, 0, (float)ClientSize.Width) - R, (float)Y[i].Map(minX2, maxX2, (float)ClientSize.Height, 0) - R, R, R);


            if (Theta == null || Regresion == null)
                return;

            //Draw hypotesis
            var points = new PointF[LinesCount];
            for (int i = 0; i < LinesCount; i++)
            {
                var x = ((double)i).Map(0, LinesCount, minX1, maxX1);
                var y = (float)Regresion.Hypothesis(new DenseVector(new[] { x }), Theta)[0];
                points[i] = new PointF((float)x.Map(minX1, maxX1, 0, (float)ClientSize.Width),
                    y.Map(minX2, maxX2, (float)ClientSize.Height, 0));
            }


            g.DrawLines(new Pen(Brushes.Green,2), points);
        }

        public PointF GetRealPointF(int x, int y)
        {
            var ratioX = x / (float)ClientSize.Width;
            var ratioY = y / (float)ClientSize.Height;
            var p = new PointF(
                minX1 * (1 - ratioX) + ratioX * maxX1,
                minX2 * ratioY + (1 - ratioY) * maxX2
                );
            return p;
        }
    }
}
