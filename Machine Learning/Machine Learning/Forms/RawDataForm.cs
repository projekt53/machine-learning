﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    public partial class RawDataForm<T> : Form where T : struct, IEquatable<T>, IFormattable
    {
        private Matrix<T> data;
        public RawDataForm(Matrix<T> data)
        {
            this.data = data;
            InitializeComponent();
            dgw.EditMode = DataGridViewEditMode.EditProgrammatically;
            for (var i = 0; i < data.ColumnCount - 1; i++)
                dgw.Columns.Add("X" + (i + 1), "X" + (i+1));
            dgw.Columns.Add("Y", "Y");

            dgw.Rows.Add(data.RowCount);
            foreach (DataGridViewRow row in dgw.Rows)
                row.HeaderCell.Value = $"{row.Index + 1}";

            for (var y = 0; y < data.RowCount; y++)
                for (var x = 0; x < data.ColumnCount; x++)
                    dgw[x, y].Value = data[y, x];
        }
    }
}
