﻿namespace Machine_Learning
{
    partial class SelectData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.b_refresh = new System.Windows.Forms.Button();
            this.b_select = new System.Windows.Forms.Button();
            this.b_rawData = new System.Windows.Forms.Button();
            this.checkBoxNormalize = new System.Windows.Forms.CheckBox();
            this.radioButtonLog = new System.Windows.Forms.RadioButton();
            this.radioButtonPoly = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.plotData2D = new Machine_Learning.PlotDataControl();
            this.textBoxAlpha = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(117, 173);
            this.listBox1.TabIndex = 1;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDoubleClick);
            // 
            // b_refresh
            // 
            this.b_refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.b_refresh.Location = new System.Drawing.Point(72, 300);
            this.b_refresh.Name = "b_refresh";
            this.b_refresh.Size = new System.Drawing.Size(57, 23);
            this.b_refresh.TabIndex = 2;
            this.b_refresh.Text = "refresh";
            this.b_refresh.UseVisualStyleBackColor = true;
            this.b_refresh.Click += new System.EventHandler(this.b_refresh_Click);
            // 
            // b_select
            // 
            this.b_select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.b_select.Location = new System.Drawing.Point(12, 329);
            this.b_select.Name = "b_select";
            this.b_select.Size = new System.Drawing.Size(117, 23);
            this.b_select.TabIndex = 2;
            this.b_select.Text = "select";
            this.b_select.UseVisualStyleBackColor = true;
            this.b_select.Click += new System.EventHandler(this.b_select_Click);
            // 
            // b_rawData
            // 
            this.b_rawData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.b_rawData.Location = new System.Drawing.Point(12, 300);
            this.b_rawData.Name = "b_rawData";
            this.b_rawData.Size = new System.Drawing.Size(54, 23);
            this.b_rawData.TabIndex = 2;
            this.b_rawData.Text = "raw";
            this.b_rawData.UseVisualStyleBackColor = true;
            this.b_rawData.Click += new System.EventHandler(this.b_rawData_Click);
            // 
            // checkBoxNormalize
            // 
            this.checkBoxNormalize.AutoSize = true;
            this.checkBoxNormalize.Checked = true;
            this.checkBoxNormalize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNormalize.Location = new System.Drawing.Point(12, 230);
            this.checkBoxNormalize.Name = "checkBoxNormalize";
            this.checkBoxNormalize.Size = new System.Drawing.Size(106, 17);
            this.checkBoxNormalize.TabIndex = 4;
            this.checkBoxNormalize.Text = "Normalize values";
            this.checkBoxNormalize.UseVisualStyleBackColor = true;
            this.checkBoxNormalize.CheckedChanged += new System.EventHandler(this.checkBoxNormalize_CheckedChanged);
            // 
            // radioButtonLog
            // 
            this.radioButtonLog.AutoSize = true;
            this.radioButtonLog.Location = new System.Drawing.Point(12, 277);
            this.radioButtonLog.Name = "radioButtonLog";
            this.radioButtonLog.Size = new System.Drawing.Size(117, 17);
            this.radioButtonLog.TabIndex = 5;
            this.radioButtonLog.Text = "Logistic Regression";
            this.radioButtonLog.UseVisualStyleBackColor = true;
            this.radioButtonLog.CheckedChanged += new System.EventHandler(this.radioButtonLog_CheckedChanged);
            // 
            // radioButtonPoly
            // 
            this.radioButtonPoly.AutoSize = true;
            this.radioButtonPoly.Checked = true;
            this.radioButtonPoly.Location = new System.Drawing.Point(12, 253);
            this.radioButtonPoly.Name = "radioButtonPoly";
            this.radioButtonPoly.Size = new System.Drawing.Size(137, 17);
            this.radioButtonPoly.TabIndex = 6;
            this.radioButtonPoly.TabStop = true;
            this.radioButtonPoly.Text = "Polynomical Regression";
            this.radioButtonPoly.UseVisualStyleBackColor = true;
            this.radioButtonPoly.CheckedChanged += new System.EventHandler(this.radioButtonPoly_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Alpha";
            // 
            // plotData2D
            // 
            this.plotData2D.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plotData2D.LinesCount = 100;
            this.plotData2D.Location = new System.Drawing.Point(138, 12);
            this.plotData2D.Name = "plotData2D";
            this.plotData2D.R = 5F;
            this.plotData2D.Size = new System.Drawing.Size(432, 341);
            this.plotData2D.TabIndex = 0;
            this.plotData2D.Theta = null;
            this.plotData2D.X = null;
            this.plotData2D.Y = null;
            // 
            // textBoxAlpha
            // 
            this.textBoxAlpha.Location = new System.Drawing.Point(12, 204);
            this.textBoxAlpha.Name = "textBoxAlpha";
            this.textBoxAlpha.Size = new System.Drawing.Size(117, 20);
            this.textBoxAlpha.TabIndex = 8;
            this.textBoxAlpha.Text = "1.41";
            this.textBoxAlpha.TextChanged += new System.EventHandler(this.textBoxAlpha_TextChanged);
            // 
            // SelectData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 364);
            this.Controls.Add(this.textBoxAlpha);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButtonPoly);
            this.Controls.Add(this.radioButtonLog);
            this.Controls.Add(this.checkBoxNormalize);
            this.Controls.Add(this.b_select);
            this.Controls.Add(this.b_rawData);
            this.Controls.Add(this.b_refresh);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.plotData2D);
            this.Name = "SelectData";
            this.Text = "SelectData";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PlotDataControl plotData2D;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button b_refresh;
        private System.Windows.Forms.Button b_select;
        private System.Windows.Forms.Button b_rawData;
        private System.Windows.Forms.CheckBox checkBoxNormalize;
        private System.Windows.Forms.RadioButton radioButtonLog;
        private System.Windows.Forms.RadioButton radioButtonPoly;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAlpha;
    }
}