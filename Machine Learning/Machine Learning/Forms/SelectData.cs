﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    public partial class SelectData : Form
    {
        public double Alpha = 1.41;
        public bool Normalize = true;
        public bool Polynomical = true;
        public bool Logistic = false;
        private string[] _paths;
        private readonly string _directory;
        public Matrix<double> SelectedData;

        public SelectData(string directory)
        {
            this._directory = directory;
            _paths = Directory.GetFiles(directory);
            InitializeComponent();
            listBox1.Items.AddRange(_paths);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedData = LoadData((string)listBox1.SelectedItem);
            GC.Collect();
            if (SelectedData == null)
            {
                plotData2D.X = null;
                plotData2D.Y = null;
                plotData2D.Invalidate();
                return;
            }

            plotData2D.X = SelectedData.RemoveColumn(SelectedData.ColumnCount-1);
            plotData2D.Y = SelectedData.Column(SelectedData.ColumnCount-1);
            plotData2D.Invalidate();
        }

        private Matrix<double> LoadData(string path)
        {
            using (var sr = new StreamReader(path))
            {
                var data = sr.ReadToEnd();
                var lines = data.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                var count = lines.First().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Length;
                var m = Matrix<double>.Build.Dense(lines.Count(), count, 0);
                var ci = CultureInfo.GetCultureInfo("en-US");

                for (var x = 0; x < lines.Count(); x++)
                {
                    var splited = lines[x].Split(',');
                    for (int y = 0; y < count; y++)
                    {
                        double num;
                        var succes = double.TryParse(splited[y], NumberStyles.Float, ci, out num);
                        if (!succes)
                            return null;
                        m[x, y] = num;
                    }
                }
                return m;
            }
        }

        private void b_refresh_Click(object sender, EventArgs e)
        {
            _paths = Directory.GetFiles(_directory);
            listBox1.Items.Clear();
            listBox1.Items.AddRange(_paths);
            plotData2D.X = null;
            plotData2D.Y = null;
            plotData2D.Invalidate();
        }

        private void b_rawData_Click(object sender, EventArgs e)
        {
            var rdf = new RawDataForm<double>(SelectedData);
            rdf.ShowDialog();
        }

        private void b_select_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var index = listBox1.IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                this.Close();
            }
        }

        private void textBoxAlpha_TextChanged(object sender, EventArgs e)
        {
            var textbox = (TextBox) sender;
            Alpha = Convert.ToDouble(textbox.Text);
        }

        private void checkBoxNormalize_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox) sender;
            Normalize = checkBox.Checked;
        }

        private void radioButtonPoly_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton) sender;
            Polynomical = radioButton.Checked;
        }

        private void radioButtonLog_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton)sender;
            Logistic = radioButton.Checked;
        }
    }
}
