﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    public class LogisticRegression
    {
        public int Exponent;
        public double Bias;
        public LogisticRegression(int exponent, double bias)
        {
            Exponent = exponent;
            Bias = bias;
        }

        public double Sigmoid(double z)
        {
            return (1.0/(1.0 + Math.Pow(Math.E, -z)));
        }

        public double Hypothesis(double bias,double x1, double x2, Vector<double> theta)
        {
            return Sigmoid(bias*theta[0] + x1*theta[1] + x2*theta[2]);
        }
        //WORKS  0.693 is correct for ex2data1
        public double Cost(Matrix<double> x, Matrix<double> y, Vector<double> theta)
        {
            double f = -(1.0 / y.RowCount);
            double sum = 0;
            for (var i = 0; i < x.RowCount; i++)
            {
                sum += y[i, 0] * Math.Log(Hypothesis(Bias,x[i, 0],x[i, 1], theta)) + (1 - y[i, 0]) * Math.Log(1 - Hypothesis(Bias, x[i, 0],x[i, 1], theta));
            }
            return f * sum;
        }

        public Vector<double> GradientDescent(Matrix<double> x, Matrix<double> y, Vector<double> theta,
            double alpha, int iterations)
        {
            double f = alpha / y.RowCount;
            for (var i = 0; i < iterations; i++)
            {
                double sum0 = 0;
                double sum1 = 0;
                double sum2 = 0;
                for (var j = 0; j < y.RowCount; j++)
                {
                    sum0 += (Hypothesis(Bias, x[j, 0], x[j, 1], theta) - y[j, 0]) * Bias;
                    sum1 += (Hypothesis(Bias, x[j, 0], x[j, 1], theta) - y[j, 0]) * x[j, 0];
                    sum2 += (Hypothesis(Bias, x[j, 0], x[j, 1], theta) - y[j, 0]) * x[j, 1];
                }
                theta[0] -= f * sum0;
                theta[1] -= f * sum1;
                theta[2] -= f * sum2;
            }
            //TODO -1 is temp
            return theta;
        }
    }
}
