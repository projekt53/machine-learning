﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    static class MatrixOperations
    {
        public static void Log(this Matrix<double> matrix)
        {
            for (var i = 0; i < matrix.ColumnCount; i++)
            {
                for (var j = 0; j < matrix.RowCount; j++)
                {
                    matrix[i, j] = Math.Log10(matrix[i, j]);
                }
            }
        }

        //todo: check for broken?
        public static Matrix<double> MatrixLog(Matrix<double> matrix)
        {
            for (var i = 0; i < matrix.ColumnCount; i++)
            {
                for (var j = 0; j < matrix.RowCount; j++)
                {
                    matrix[i, j] = Math.Log10(matrix[i, j]);
                }
            }
            return matrix;
        }

        public static Matrix<double> MatrixPowTwo(Matrix<double> matrix)
        {
            for (var i = 0; i < matrix.ColumnCount; i++)
            {
                for (var j = 0; j < matrix.RowCount; j++)
                {
                    matrix[i, j] = Math.Pow(matrix[i, j],2);
                }
            }
            return matrix;
        }

        public static Matrix<double> VectorToMatrix(Vector<double> vector)
        {
            var m = Matrix<double>.Build.Dense(vector.Count, 1, 0);
            for (var i = 0; i < vector.Count; i++)
            {
                m[i, 0] = vector[i];
            }
            return m;
        }

    }
}
