﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
namespace Machine_Learning
{
    class LineralRegression
    {
        public static Matrix<double> GradientDescent(Matrix<double>x,Matrix<double>y,Matrix<double> theta, double alpha, int iterations)
        {
            double m = y.RowCount;
            var bias = Matrix<double>.Build.Dense(x.RowCount, 1, 1);
            for (var i = 0; i < iterations; i++)
            {
                double sum0 = 0;
                double sum1 = 0;
                for (var j = 0; j < y.RowCount; j++)
                {
                    sum0 += (Hypothesis(x[j,0], theta) - y[j,0]) * bias[j,0];
                    sum1 += (Hypothesis(x[j,0], theta) - y[j, 0]) * x[j,0];
                }
                theta[0,0] = theta[0,0]- (alpha * (1 / m) * sum0);
                theta[1,0]= theta[1, 0] - (alpha * (1 / m) * sum1);
            }
            return theta;
        }

        public static double Hypothesis(double x, Matrix<double> theta)
        {
            return theta[0,0] + theta[1,0]* x;
        }

        public static double Cost(Matrix<double> x, Matrix<double> y, Matrix<double> theta)
        {
            double f = 2 * y.RowCount;
            var s = 1 / f;
            double sum = 0;
            for (var i = 0; i < y.RowCount; i++)
            {
                sum += Math.Pow(Hypothesis(x[i, 0], theta) - y[i, 0], 2);
            }
            return s * sum;
        }
    }
}
