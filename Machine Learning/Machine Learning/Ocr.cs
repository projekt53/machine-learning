﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    internal class Ocr
    {
        private List<Vector<double>> ThetaList;

        public Ocr()
        {
            CostFunction(Matrix<double>.Build.Dense(25, 785, 0), Matrix<double>.Build.Dense(10, 26, 0), 784, 25, 9, Matrix<double>.Build.Dense(5000, 784, 0), Matrix<double>.Build.Dense(5000, 10, 0), 0);
        }
        public Vector<double> CostFunction(Matrix<double> theta1, Matrix<double> theta2, int inputLayerSize, int hiddenLayerSize, int numLabels, Matrix<double> x, Matrix<double> y, int lambda)
        {
            var a1 = Matrix<double>.Build.Dense(5000, 1,1);
            a1 = a1.Append(x);
            //a1 5000x401
            var z2 = a1*theta1.Transpose();
            //5000x25
            var a2 = Sigmoid(z2);
            //5000x26
            a2 = Matrix<double>.Build.Dense(a2.RowCount, 1, 1).Append(a2);
            //5000x26  (25+1)
            var z3 = a2*theta2.Transpose();
            //5000x10
            var a3 = Sigmoid(z3);
            //5000x10
            //transpose jede, .* != * --> je treba je nasobit individualne ne *
            var cost = (a3.PointwiseMultiply(-y) - (1 - y).PointwiseMultiply((1-a3).PointwiseLog())).RowSums();
            // ReSharper disable once PossibleLossOfFraction
            var J = 1/x.RowCount*cost.Sum();
            //TODO regularization
            //var delta1 = Matrix<double>.Build.Dense(numLabels+1, hiddenLayerSize+1, 0);
            //var delta2 = Matrix<double>.Build.Dense(hiddenLayerSize, inputLayerSize + 1, 0);
            //TODO this might cause issues (we assume bias is 1st column)
            var theta1NoBias = theta1.RemoveColumn(0);
            var theta2NoBias = theta2.RemoveColumn(0);
            //reg = (lambda / (2 * m)) * (sum(sumsq(Theta1NoBias)) + sum(sumsq(Theta2NoBias)));
            //J += reg;
            Matrix<double> delta1 = null;
            Matrix<double> delta2 = null;
            for (int i = 0; i < x.RowCount; i++)
            {
                var biasMatrix = Matrix<double>.Build.Dense(1, 1, 1);
                //var test = MatrixOperations.VectorToMatrix(x.Row(i)).Transpose();
                a1 = Matrix<double>.Build.Dense(1, 1, 1).Append(MatrixOperations.VectorToMatrix(x.Row(i)).Transpose()).Transpose();
                z2 = theta1*a1;
                //ADD bias to 25(row)x1 matrix --> 26x1 starting with bias, not working
                //a2 = Matrix<double>.Build.Dense(1, 1, 1).Append(Sigmoid(z2));
                a2 = Sigmoid(z2).InsertRow(0, Vector<double>.Build.Dense(1,1));
                z3 = theta2*a2;
                a3 = Sigmoid(z3);
                var d3 = a3 - y.Row(i).ToRowMatrix().Transpose();
                var d2 = (theta2NoBias.Transpose()*d3).PointwiseMultiply(SigmoidGradient(z2));
                if (delta1 == null)
                {
                    delta1 = (d2*a1.Transpose());
                }
                else
                {
                    delta1 += (d2*a1.Transpose());
                }
                if (delta2 == null)
                {
                    delta2 = (d3*a2.Transpose());
                }
                else
                {
                    delta2 += (d3*a2.Transpose());
                }
            }
            // ReSharper disable once PossibleLossOfFraction
            var theta1Grad = 1 / x.RowCount * delta1;
            // ReSharper disable once PossibleLossOfFraction
            var theta2Grad = 1 / x.RowCount * delta2;
            //TODO regularized gradient
            //Theta1_grad(:, 2:end) += ((lambda / m) * Theta1NoBias);
            //Theta2_grad(:, 2:end) += ((lambda / m) * Theta2NoBias);
            //TODO might cause issue  http://puu.sh/sFdHI/49c816aa09.png
            var first = theta1Grad.ToColumnWiseArray();
            var second = theta2Grad.ToColumnWiseArray();
            var grad = Vector<double>.Build.DenseOfArray(first.Concat(second).ToArray());
            return grad; 
        }



        public static Matrix<double> Sigmoid(Matrix<double> z)
        {

            for (int i = 0; i < z.ColumnCount; i++)
            {
                for (int j = 0; j < z.RowCount; j++)
                {
                    z[j, i] = Math.Pow(Math.E, -z[j, i]);
                }
            }
            return 1 / (1 + z);
        }

        public static Matrix<double> SigmoidGradient(Matrix<double> z)
        {
            var sig = Sigmoid(z);
            return sig.PointwiseMultiply(1 - sig);
            
        }

        //private List<Image> _images; 
        //public void LoadPictures(string path)

        public Ocr(List<int> architecture, Matrix<double> X, Vector<double> Y, int outputNumbers)
        {
            //Init Theta
            ThetaList = new List<Vector<double>>();
            ThetaList.Add(Vector<double>.Build.Dense(X.RowCount+1)); //InputLayer
            foreach (var arch in architecture)
                ThetaList.Add(Vector<double>.Build.Dense(arch+1)); //HiddenLayers
            ThetaList.Add(Vector<double>.Build.Dense(outputNumbers)); //OutputLayer

            //Make Theta random again
            var r = new Random();
            for (int i = 0; i < ThetaList.Count; i++)
                for (int j = 0; j < ThetaList[i].Count; j++)
                    ThetaList[i][j] = r.NextDouble();

            //TODO: save important variables such as Y
            //CostFunction(MatrixOperations.VectorToMatrix(ThetaList[0]), MatrixOperations.VectorToMatrix(ThetaList[1]), 784, 25, 9, Matrix<double>.Build.Dense(5000, 784, 0), Matrix<double>.Build.Dense(5000, 10, 0), 0);
        }

    }


    //private List<Image> _images;
    //public void LoadPictures(string path)
    //{
    //    var images = Directory.GetFiles(path, "*.jpg");
    //    foreach (var foundImage in images)
    //    {
    //        var image = Image.FromFile(foundImage);
    //        //var test = image.Length;
    //        image = ResizeImage(image, new Size(20, 20));
    //        var bitmap = new Bitmap(image);
    //        image = (Image)BlackAndWhite(bitmap);
    //        _images.Add(image);
    //    }
    //}
    //public static Image ResizeImage(Image image, Size size)
    //{
    //    return (Image)(new Bitmap(image, size));
    //}
    //public Bitmap BlackAndWhite(Bitmap bitmap)
    //{

    //    var output = new Bitmap(bitmap.Width, bitmap.Height);
    //    for (int i = 0; i < bitmap.Width; i++)
    //    {
    //        for (int j = 0; j < bitmap.Height; j++)
    //        {
    //            var c = bitmap.GetPixel(i, j);
    //            var average = ((c.R + c.B + c.G) / 3);
    //            output.SetPixel(i, j, average < 200 ? Color.Black : Color.White);
    //        }
    //    }
    //    return output;

    //}


    //public class ConvertedImage
    //{
    //    private byte[] _imageBytes;

    //    public ConvertedImage(byte[] imageBytes)
    //    {
    //        this._imageBytes = imageBytes;
    //    }
    //}
}
