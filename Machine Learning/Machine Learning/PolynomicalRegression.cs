﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Machine_Learning
{
    public class PolynomicalRegression : Regresion
    {
        public Matrix<double> X;
        public Vector<double> Y;
        public Vector<double> Theta;
        //TODO add select
        public int Exponent = 10;


        public PolynomicalRegression(Matrix<double> matrix)
        {
            if(matrix.ColumnCount < 2)
                throw new Exception("Matrix is too small");
            if(matrix.ColumnCount > 2)
                throw new NotSupportedException(); //TODO: make support for x2, x3

            X = matrix.RemoveColumn(matrix.ColumnCount-1);
            Y = matrix.Column(matrix.ColumnCount - 1);
            Theta = new DenseVector(Exponent);
        }

        public PolynomicalRegression(Matrix<double> x, Vector<double> y)
        {
            if (x.ColumnCount < 1)
                throw new Exception("Matrix is too small");
            if (x.ColumnCount > 1)
                throw new NotSupportedException(); //TODO: make support for x2, x3

            X = x;
            Y = y;
            
            Theta = new DenseVector(Exponent);
        }
        
        /// <returns>Returns Theta vector </returns>
        public Vector<double> Fit(double alpha, int iterations)
        {
            var mOverOne = 1 / (double)Y.Count;

            for (var i = 0; i < iterations; i++)
            {
                var sums = new double[Exponent];

                for (var j = 0; j < Y.Count; j++)
                {
                    for (var k = 0; k < Exponent; k++)
                    {
                        var hyp = Hypothesis(new DenseVector(new[] {X[j, 0]}), Theta)[0];
                        sums[k] += (hyp - Y[j]) *Math.Pow(X[j, 0], Math.Ceiling((double) k));
                    }
                }
                for (var k = 0; k < Exponent; k++)
                {
                    Theta[k] -= alpha * mOverOne * sums[k];
                }
            }
            return Theta;
        }

        public async Task<Vector<double>> FitAsync(double alpha, int iterations)
        {
            Task<Vector<double>> task = new Task<Vector<double>>(() =>
            {
                var mOverOne = 1/(double) Y.Count;

                for (var i = 0; i < iterations; i++)
                {
                    var sums = new double[Exponent];

                    for (var j = 0; j < Y.Count; j++)
                    {
                        for (var k = 0; k < Exponent; k++)
                            sums[k] += (Hypothesis(new DenseVector(new[] {X[j, 0]}), Theta)[0] - Y[j])*
                                       Math.Pow(X[j, 0], Math.Ceiling((double) k));
                    }
                    for (var k = 0; k < Exponent; k++)
                    {
                        Theta[k] -= alpha*mOverOne*sums[k];
                    }
                }
                return Theta;
            });
            task.Start();
            await task;
            return task.Result;
        }

        public override Vector<double> Hypothesis(Vector<double> xrow, Vector<double> theta)
        {
            if(xrow.Count!=1)
                throw new NotSupportedException();

            Vector<double> num = new DenseVector(1);

            for (var i = 0; i < Exponent; i++)
                num[0] += theta[i] * Math.Pow(xrow[0], i);

            return num;
        }

        public double Cost(Matrix<double> x, Vector<double> y, Vector<double> theta)
        {
            double f = 2 * y.Count;
            var s = 1.0 / f;
            double sum = 0;
            for (var i = 0; i < y.Count; i++)
            {
                
                sum += Math.Pow(Hypothesis(new DenseVector(new[] { x[i, 0] }), theta)[0] - y[i], 2);
            }
            return s * sum;
        }
    }
}

