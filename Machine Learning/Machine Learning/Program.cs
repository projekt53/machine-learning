﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var sd = new SelectData("data/");
                sd.ShowDialog();
                var m = sd.SelectedData;
                var alpha = sd.Alpha;
                var normalize = sd.Normalize;
                var polynomical = sd.Polynomical;
                var logistic = sd.Logistic;
                if (normalize)
                {
                    try
                    {
                        m = FeatureNormalization.NormalizeFeatures(m);
                    }
                    catch (Exception)
                    {
                    }

                }
                if (polynomical)
                {
                    try
                    {
                        var pol = new PolynomicalRegression(m);
                        var dp = new DataPlotter(m, pol);
                        new Thread
                            (() => { dp.ShowDialog(); }).Start();

                        while (true)
                        {
                            var reg = pol.Fit(alpha, 1);
                            dp.UpdateTheta(reg);
                        }
                    }
                    catch (Exception)
                    {
                    }
                  
                }
                else if (logistic)
                {
                    //TODO
                    var log = new LogisticRegression(10, 1);
                }
            }

        }
    }
}
