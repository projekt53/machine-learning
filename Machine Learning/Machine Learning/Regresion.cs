﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Machine_Learning
{
    public abstract class Regresion
    {
        public abstract Vector<double> Hypothesis(Vector<double> xrow, Vector<double> theta);
    }
}
