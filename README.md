# Polynomial regression#
## GLOBAL LAND-OCEAN TEMPERATURE ##
![rename.png](https://bitbucket.org/repo/nzgx5k/images/3362966085-rename.png)
[Data source: NASA's Goddard Institute for Space Studies (GISS).](http://climate.nasa.gov/vital-signs/global-temperature/)

## Carbon Dioxide PPM levels ##
![co2.png](https://bitbucket.org/repo/nzgx5k/images/2810167714-co2.png)
[Data source: Monthly measurements (average seasonal cycle removed). Credit: NOAA](http://climate.nasa.gov/vital-signs/carbon-dioxide/)

## Sample data ##
![sample.png](https://bitbucket.org/repo/nzgx5k/images/2345353191-sample.png)